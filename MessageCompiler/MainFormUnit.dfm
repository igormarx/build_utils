object MainForm: TMainForm
  Left = 532
  Top = 236
  Width = 726
  Height = 257
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 8
    Top = 16
    Width = 97
    Height = 25
    Caption = 'Make'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 120
    Top = 16
    Width = 585
    Height = 193
    Lines.Strings = (
      'Memo1')
    TabOrder = 1
    WordWrap = False
  end
  object Button2: TButton
    Left = 8
    Top = 96
    Width = 97
    Height = 25
    Caption = 'Parse messages'
    TabOrder = 2
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 8
    Top = 56
    Width = 97
    Height = 25
    Caption = 'Encode To MP3'
    TabOrder = 3
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 8
    Top = 136
    Width = 97
    Height = 25
    Caption = 'Write Test.wav'
    TabOrder = 4
    OnClick = Button4Click
  end
  object btCompile: TButton
    Left = 8
    Top = 176
    Width = 97
    Height = 25
    Caption = 'Compile'
    TabOrder = 5
    OnClick = btCompileClick
  end
end
