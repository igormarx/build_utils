#ifndef __USBFIND_H_
#define __USBFIND_H_
#include <vector>

typedef struct
{
	wchar_t driveLetter;
	bool readonly;
	std::wstring Device_EnumeratorName;
	std::wstring Device_Path;
	std::wstring DosDeviceName;
	std::wstring Device_DeviceDesc;
	std::wstring Device_Service;
	std::wstring Device_Class;
	std::wstring Device_Driver;
	std::wstring Device_Manufacturer;
	std::wstring Device_FriendlyName;
	std::wstring Device_PDOName;
} usbdisk_t;

int win32_enum_usb_disks(std::vector<usbdisk_t> & device_list);


#endif
