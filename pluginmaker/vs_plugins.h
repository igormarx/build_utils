#ifndef __VS_PLUGINS_H_
#define __VS_PLUGINS_H_

#define MPLAB_C30_LONG_ARRAY	0

#if MPLAB_C30_LONG_ARRAY
// if -mlarge-arrays compiler option used
// not tested!
typedef WORD tPIC24PointerHolder;
typedef DWORD tPIC24size_t;
#else
typedef unsigned short tPIC24PointerHolder;
typedef unsigned short tPIC24size_t;
#endif

typedef struct {
	struct
	{
		tPIC24PointerHolder atable;
		tPIC24PointerHolder dtable;
		tPIC24size_t arraysize;
	} elements [12];
} tPluginTableFlash, *pPluginTableFlash;


#define PLUGIN_TYPE_END_OF_LIST			-1
#define PLUGIN_TYPE_UNCOMPRESSED_PLUGIN	1
#define PLUGIN_TYPE_COMPRESSED_PLUGIN	2
typedef struct
{
	unsigned long offset;
	unsigned long arraysize:24;
	unsigned long type:8;
} tPluginDirEepromElement, *pPluginDirEepromElement;


#ifdef __cplusplus
extern "C" {
#endif

#define PATCH_W_FLAC_PLUGIN_SIZE 6714	/*renewed 04-aug-2011, ver 180 */
extern const unsigned short patch_w_flac[PATCH_W_FLAC_PLUGIN_SIZE];

#define IMA_ENCCODER_PATCH_SIZE	40
extern const unsigned short ima_encoder_fix[IMA_ENCCODER_PATCH_SIZE];

#define TEMPOCHANGER_SIZE 1332
extern const unsigned char atempochanger[TEMPOCHANGER_SIZE];
extern const unsigned short dtempochanger[TEMPOCHANGER_SIZE];


// update 1-oct-2010
// update 04-aug-2011

#define venc44k2q05_SIZE 13713 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc44k2q05[venc44k2q05_SIZE];

#define venc16k1q05_SIZE 12920 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc16k1q05[venc16k1q05_SIZE];

#define venc16k1q10_SIZE	12920 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc16k1q10[venc16k1q10_SIZE];

#define venc8k1q05_SIZE		12865 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc8k1q05[venc8k1q05_SIZE];

#define venc44k2q00_SIZE	13876 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc44k2q00[venc44k2q00_SIZE];

#define venc16k2q05_SIZE	13587 /*renewed 04-aug-2011, ver 170b */
extern const unsigned short venc16k2q05[venc16k2q05_SIZE];

#define admixstereo_SIZE	117
extern const unsigned short admixstereo[admixstereo_SIZE];

#ifdef __cplusplus
}
#endif

#endif
