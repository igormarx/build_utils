program MP3MessageCompiler;

uses
  ExceptionLog,
  Forms, SysUtils, Windows, Dialogs,
  MainFormUnit in 'MainFormUnit.pas' {MainForm},
  EG_DTBGogoUtils in 'EG_DTBGogoUtils.pas';

{$R *.res}

var i:integer;
	s,s1:string;
    error:boolean;
    infile,hfile,outfile:string;
begin
	Application.Initialize;
	i := 0;
    s := ParamStr(i);
    infile := '';
    hfile := '';
    outfile := '';
    error := false;
    ExitCode := 1;
    //ShowMessage(ParamStr(0));
    repeat
		inc(i);
    	s := ParamStr(i);
        if length(s) = 0 then break;
        //ShowMessage(s);
      	s1 := Copy(s,1,1);
        if s1 = '/' then
        begin
        	s1 := copy(s,1,2);
            s := copy(s,4,MAXINT);
            if (copy(s,1,1) = '"') and (copy(s,length(s),1) = '"')
            then s := Copy(s,2,length(s)-2);
            if s1 = '/i' then infile := s else
            if s1 = '/o' then outfile := s else
            if s1 = '/h' then hfile := s else
            error := true;
        end else
        begin
            infile := s;
            outfile := ChangeFileExt(s,'.bin');
            hfile := ChangeFileExt(s,'.h');
            break;
        end;
    until error;
    if (length(infile) <> 0) and (length(outfile) <> 0) and (length(hfile) <> 0) then
    begin
    	try
	        CompileMessages(infile,hfile,outfile,nil);
            ExitCode := 0;
        except on e:exception do
            begin
                MessageBox(0,pchar(e.Message),'MessageCompiler',
                    MB_OK or MB_ICONSTOP);
                ExitCode := 1;
            end
        end;
    end else
    begin
		Application.CreateForm(TMainForm, MainForm);
		Application.Run;
    end;
end.
