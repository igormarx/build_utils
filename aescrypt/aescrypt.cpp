#include "stdafx.h"

#include "rijndael/rijndael.h"

typedef std::basic_string<TCHAR>	t_string;
typedef std::basic_string<wchar_t>	w_string;
typedef std::basic_string<char>		a_string;
typedef std::basic_istream<TCHAR, std::char_traits<TCHAR> > t_istream;
typedef std::basic_ifstream<TCHAR, std::char_traits<TCHAR> > t_ifstream;
typedef std::basic_ofstream<TCHAR, std::char_traits<TCHAR> > t_ofstream;

#ifdef UNICODE

#define cout	std::wcout
#define cerr	std::wcerr

#else
#define cout	std::cout
#define cerr	std::cerr
#endif

#define WAITKEY() if(bPause)getchar()

#define AES_KEYSIZE 16
#define AES_BLOCKSIZE 16

typedef char tKEY[AES_KEYSIZE];
typedef char tBLOCK[AES_BLOCKSIZE];
typedef tBLOCK* pBLOCK;
typedef tKEY* pKEY;

tKEY key;
tBLOCK vector;
CRijndael oRijndael;
bool bPause = false;

bool bKeyInitialized = false;
bool bUseVector = false;
t_string infile, outfile, tempfile;

enum {
	eNoAction,
	eEncrypt,
	eDecrypt,
} enAction = eNoAction;

bool CheckOption (const TCHAR* ptr, const TCHAR option, bool bColonNeeded)
{
	TCHAR c = *(ptr++);
	if (c != '-' && c != '/')
		return false;
	c = *(ptr++);
	if (c > 0x80)
		return false;
	if ((c != option) && (toupper(c) != option))
		return false;
	return (!bColonNeeded) || (':' == *(ptr));
}

bool GetSymbol(TCHAR* b, const TCHAR* p)
{
	TCHAR h1=toupper(p[0]);
	TCHAR h2=toupper(p[1]);
	bool Result=true;
	if (h1<'0') Result=false; 
	else if (h1 <= '9') h1=h1-'0'; 
	else if (h1 < 'A') Result=false; 
	else if (h1 <= 'F') h1=h1-'A'+10;

	if (h2 < '0') Result=false;
	else if (h2 <= '9') h2=h2-'0';
	else if (h2 < 'A') Result=false;
	else if (h2 <= 'F') h2=h2-'A'+10;
	if (Result) *b=(h1 << 4) | h2;
	return true;
}

bool KeyFromHex(TCHAR* key_str, pKEY key)
{
	size_t i,n = _tcslen(key_str);
	TCHAR b;
	if (n != AES_KEYSIZE*2)
		return false;
	for (i = 0; i < AES_KEYSIZE; i++)
	{
		if (!GetSymbol(&b, key_str))
			return false;
		key_str +=2;
		((BYTE*)key)[i] = (BYTE)b;
	}
	return true;
}


bool parse_cmd(int argc, TCHAR* argv[])
{
	for( int i = 1; i < argc; i++)
	{
		if (CheckOption(argv[i],'i',true)) // input file
		{
			if (infile.length() == 0)
				infile = &argv[i][3];
			else
			{
				cerr << TEXT("Error: only one input file allowed\n");
				return false;
			}
		} else if (CheckOption(argv[i],'o',true)) // input file
		{
			if (outfile.length() == 0)
				outfile = &argv[i][3];
			else
			{
				cerr << TEXT("Error: only one output file allowed\n");
				return false;
			}
		} else if (CheckOption(argv[i],'d', false)) // decrypt
		{
			if (enAction != eNoAction)
			{
				cerr << TEXT("Only one option -d or -e must be used\n");
				return false;
			}
			enAction = eDecrypt;
		} else if (CheckOption(argv[i],'e', false)) // encrypt
		{
			if (enAction != eNoAction)
			{
				cerr << TEXT("Only one option -d or -e must be used\n");
				return false;
			}
			enAction = eEncrypt;
		} else if (CheckOption(argv[i],'v', false)) // encrypt
		{
			bUseVector = true;
		} else if (CheckOption(argv[i],'k', true)) // key
		{
			if (!KeyFromHex(&argv[i][3],&key))
			{
				cerr << TEXT("Error: bad key %s specified (use like -k:0123456789ABCDEF0123456789ABCDEF)\n");
				return false;
			}
			bKeyInitialized = true;
		} else if (CheckOption(argv[i],'p', false)) // key
		{
			bPause = true;
		}
		else 
			cerr << TEXT("Unknown option: ") << argv[i] << TEXT("\n");
	}
	if (infile.length() == 0)
	{
		cerr << TEXT("Input filename empty\n");
		return false;
	}
	if (outfile.length() == 0)
	{
		cerr << TEXT("Output filename empty\n");
		return false;
	}
	if(!bKeyInitialized)
	{
		cerr << TEXT("No encryption key specified\n");
		return false;
	}
	if (enAction == eNoAction)
	{
		cerr << TEXT("Use -d or -e option\n");
		return false;
	}
	return true;
}

void help()
{
	cerr << 
		TEXT("File encrypt utility. Usage: aescrypt.exe <option1> <option2> ...\n\n")
		TEXT("Options:\n")
		TEXT("-i:<filename>   input file\n")
		TEXT("-o:<varname>    output file\n")
		TEXT("-d              decrypt\n")
		TEXT("-e              encrypt\n")
		TEXT("-v              CBC (random generated vector stored as first block in target file)\n")
		TEXT("-k              key (hexadecimal string)\n")
		TEXT("-p              pause on error\n")
		TEXT("(c) Khlebnikov Igor, 2009.\n\n");
}

bool EncryptMyStreams(std::ifstream& ifile, std::ofstream& ofile, bool bUseVector,tKEY key)
{
	tBLOCK buffer, out_buffer, chain;
	std::streamsize was_read;
	if (bUseVector)
	{
#ifdef _DEBUG
		memset(chain, 0x55, AES_BLOCKSIZE);
#else
		HRESULT hr = ::UuidCreate((UUID*)chain);
		if (HRESULT_CODE(hr) != RPC_S_OK)
		{
			cerr << TEXT("Cannot initialize vector\n");
			WAITKEY();
			return false;
		}
#endif
		ofile.write(chain,AES_BLOCKSIZE);
	}
	
	oRijndael.MakeKey((char *)key, chain);
	while (!ofile.bad())
	{
		ifile.read((char*)buffer, AES_BLOCKSIZE);
		if (ifile.bad())
		{
			cerr << TEXT("Error: cannot read input file\n");
			WAITKEY();
			return false;
		}
		was_read = ifile.gcount();
		if(!was_read)
			return true; // eof
		if (was_read < AES_BLOCKSIZE)
			memset(buffer + was_read, 0, AES_BLOCKSIZE - was_read);
			
		oRijndael.Encrypt((char*)buffer, (char*)out_buffer, AES_BLOCKSIZE, bUseVector ? CRijndael::CBC : CRijndael::ECB);
		ofile.write((char*)out_buffer, AES_BLOCKSIZE);
	}
	cerr << TEXT("Encrypt error");
	return false;
}

bool DecryptMyStreams(std::ifstream& ifile, std::ofstream& ofile, bool bUseVector,tKEY key)
{
	bool bFirstBlock = bUseVector;
	tBLOCK buffer, out_buffer, chain;
	std::streamsize was_read;
	if (bUseVector)
	{
		ifile.read((char*)chain, AES_BLOCKSIZE);
		if (ifile.bad())
		{
			cerr << TEXT("Error: cannot read vector from input file\n");
			return false;
		}
		was_read = ifile.gcount();
		if (was_read != AES_BLOCKSIZE)
		{
			cerr << TEXT("Error: file does not appear as CBC encoded (too short)\n");
			return false;
		}
	}

	oRijndael.MakeKey((char *)key, chain);

	while (!ofile.bad())
	{
		ifile.read((char*)buffer, AES_BLOCKSIZE);
		if (ifile.bad())
		{
			cerr << TEXT("Error: cannot read input file\n");
			return false;
		}
		was_read = ifile.gcount();
		if(!was_read)
			return true;
		if (was_read < AES_BLOCKSIZE)
		{
			cerr << TEXT("Error: file size is not a product of cipher block length\n");
			return false;
		}
		oRijndael.Decrypt((char*)buffer, (char*)out_buffer, AES_BLOCKSIZE, bUseVector ? CRijndael::CBC : CRijndael::ECB);
		ofile.write(out_buffer, AES_BLOCKSIZE);
	}
	cerr << TEXT("Decrypt error");
	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{
	if ((argc < 3) || !parse_cmd(argc,argv))
	{
		help();
		WAITKEY();
		return 1;
	}
	std::ifstream ifile;
	std::ofstream ofile;
	tempfile = outfile + TEXT(".~");
	ifile.open(infile.c_str(), std::ios_base::in | std::ios_base::binary);
	if (!ifile.is_open())
	{
		cerr << TEXT("Error: the input file ") <<  infile.c_str() << TEXT(" does not exist\n");
		WAITKEY();
		return 1;
	}
	ofile.open(tempfile.c_str(), std::ios_base::out | std::ios_base::binary);
	if (!ofile.is_open())
	{
		cerr << TEXT("Error: cannot create temporary file ") <<  tempfile.c_str() << TEXT("\n");
		WAITKEY();
		return 1;
	}
	bool bResult = (enAction == eEncrypt) ? EncryptMyStreams(ifile, ofile, bUseVector, key)
		: DecryptMyStreams(ifile, ofile, bUseVector, key);
	
	// encrypt/decrypt cycle


	ifile.close();
	ofile.close();
	if (ofile.bad())
		cerr << TEXT("Error: cannot write to output file\n");
	if(bResult)
	{
		if(!CopyFile(tempfile.c_str(),outfile.c_str(),FALSE))
		{
			DeleteFile(tempfile.c_str());
			cerr << TEXT("Error: rewrite old output file\n");
			WAITKEY();
			return 1;
		}
		DeleteFile(tempfile.c_str());
		return 0;
	} else DeleteFile(tempfile.c_str());
	WAITKEY();
	cerr << TEXT("Encrypt error\n");
	return 1;
}

