//#include "windows.h"
#include "stdafx.h"
#include "windows.h"
#include <assert.h>
#include <ctype.h>

typedef unsigned char bool;
#define true (1==1)
#define false (1==0)

char hex(unsigned char b)
{
	b &=0x0f;
	if (b<10)
		return b+'0';
	return b-10+'a';
}

bool hex_write_byte(HANDLE h, unsigned char b, unsigned char* checksum)
{
	char buffer[2];
	unsigned long Written;
	buffer[0] = hex(b >> 4);
	buffer[1] = hex(b);
	WriteFile(h,&buffer,2,&Written,NULL);
	*checksum += b;
	return Written==2;
}
bool hex_write_word(HANDLE h, WORD w, unsigned char* checksum)
{
	return hex_write_byte(h, (unsigned char)(w >> 8), checksum) && hex_write_byte(h,(unsigned char)w, checksum);
}

bool hex_write_char(HANDLE h, char c)
{
	unsigned long Written;
	WriteFile(h,&c,1,&Written,NULL);
	return Written == 1;
}
bool hex_write_crlf(HANDLE h)
{
	return hex_write_char(h,'\xd') && hex_write_char(h,'\xa');
}

bool hex_write_extended_addr_record(HANDLE h, WORD addr)
{
	//:02 0000 04 0000 fa
	unsigned char checksum = 0;
	if (!hex_write_char(h,':'))
		return false;
	if (!hex_write_byte(h,2,&checksum))
		return false;
	if (!hex_write_word(h,0,&checksum))
		return false;
	if (!hex_write_byte(h,4,&checksum))
		return false;
	if (!hex_write_word(h,addr,&checksum))
		return false;
	if (!hex_write_byte(h,(BYTE)(0x100-checksum),&checksum))
		return false;
	return hex_write_crlf(h);
}
bool hex_write_eof(HANDLE h)
{
	char eof[] = ":00000001FF";
	unsigned long Written;
	WriteFile(h,eof,sizeof(eof)-1,&Written,NULL);
	return Written==sizeof(eof)-1;
}

unsigned int CheckHighAddrChange(unsigned int paddr, unsigned int inc)
{
	unsigned int retval = 0;
	while (inc--)
	{
		retval++;
		paddr++;
		if ((paddr & 0xffff) == 0)
			break;
	}
	return retval;
}

bool export_intel_hex_pic24_constdata(LPCTSTR filename,unsigned long hex_start,unsigned char* p, unsigned long areasize)
{
	HANDLE h;
	unsigned long addr = hex_start;
	bool result;
	bool addr_shall_change = true;
	unsigned char checksum;
	unsigned long chars_to_export,chars_to_export_max,words_to_export;
	unsigned long i;
	
	assert((areasize % 2)==0);
	result =  false;
	h = CreateFile(filename,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (h != INVALID_HANDLE_VALUE)
	{
		while(areasize)
		{
			if(addr_shall_change)
			{
				addr_shall_change = false;
				if(!hex_write_extended_addr_record(h,addr>>16))
					break;
			}
			checksum = 0;
			chars_to_export = 8;
			if(chars_to_export > areasize)
				chars_to_export = areasize;

			chars_to_export_max = CheckHighAddrChange(addr,chars_to_export+1);

			if(chars_to_export_max < chars_to_export+1)
			{
				addr_shall_change = true;
				chars_to_export = chars_to_export_max;
			}
			words_to_export = chars_to_export >> 1;

			if(!hex_write_char(h,':'))
				break;
			if(!hex_write_byte(h,(unsigned char)chars_to_export,&checksum))
				break;
			if(!hex_write_word(h,(WORD)addr,&checksum))
				break;
			for(i=0; i<words_to_export; i++)
			{
				if(!(result = hex_write_byte(h,*(p++),&checksum)))
					break;
				if(!(result = hex_write_byte(h,*(p++),&checksum)))
					break;
				if(!(result = hex_write_byte(h,0,&checksum)))
					break;
				if(!(result = hex_write_byte(h,0,&checksum)))
					break;
				addr+=2;
				areasize-=2;
			}
			if(!(result = hex_write_byte(h,(BYTE)(0x100-checksum),&checksum)))
				break;
			if(!(result = hex_write_crlf(h)))
				break;
		}
		CloseHandle(h);
		return (areasize == 0);
	}
	return false;
}

bool export_intel_hex(LPCTSTR filename,unsigned long hex_start,unsigned char* p, unsigned long areasize)
{
	HANDLE h;
	unsigned long addr = hex_start;
	bool result;
	bool addr_shall_change = true;
	unsigned char checksum;
	unsigned long chars_to_export,chars_to_export_max;
	unsigned long i;
	
	result =  false;
	h = CreateFile(filename,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,FILE_ATTRIBUTE_NORMAL,NULL);
	if (h != INVALID_HANDLE_VALUE)
	{
		while(areasize)
		{
			if(addr_shall_change)
			{
				addr_shall_change = false;
				if(!hex_write_extended_addr_record(h,addr>>16))
					break;
			}
			checksum = 0;
			chars_to_export = 16;
			if(chars_to_export > areasize)
				chars_to_export = areasize;

			chars_to_export_max = CheckHighAddrChange(addr,chars_to_export+1);

			if(chars_to_export_max < chars_to_export+1)
			{
				addr_shall_change = true;
				chars_to_export = chars_to_export_max;
			}
			
			if(!hex_write_char(h,':'))
				break;
			if(!hex_write_byte(h,(unsigned char)chars_to_export,&checksum))
				break;
			if(!hex_write_word(h,(WORD)addr,&checksum))
				break;
			if(!hex_write_byte(h,0,&checksum))
				break;
			for(i=0; i<chars_to_export; i++)
			{
				if(!(result = hex_write_byte(h,*(p++),&checksum)))
					break;
				addr++;
				areasize--;
			}
			if(!(result = hex_write_byte(h,(BYTE)(0x100-checksum),&checksum)))
				break;
			if(!(result = hex_write_crlf(h)))
				break;
		}
		if(result)
			result = hex_write_eof(h);
		CloseHandle(h);
		return (areasize == 0);
	}
	return false;
}