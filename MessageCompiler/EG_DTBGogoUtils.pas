unit EG_DTBGogoUtils;

interface

uses
  Windows, GogoDefs, GogoUtils;

type
  TEncodeWavToMP3ProgressEvent = procedure (Sender: TObject; PercentDone: Integer) of object;

type
  TDTB_MP3Encoder = class(TObject)
  private
    FOnMP3Progress: TEncodeWavToMP3ProgressEvent;
    procedure SetOnMP3Progress(const Value: TEncodeWavToMP3ProgressEvent);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure EncodeWavToMP3(WavFileName: String; MP3FileName: String; DoubleSpeed: Boolean);
    property OnMP3Progress: TEncodeWavToMP3ProgressEvent read FOnMP3Progress write SetOnMP3Progress;
  end;

implementation

procedure TDTB_MP3Encoder.EncodeWavToMP3(WavFileName: String; MP3FileName: String; DoubleSpeed: Boolean);
var
  frameCount: DWORD;
  frameIdx: DWORD;
  rVal: MERET;
begin
  MPGECheck(MPGE_initializeWork);
  try
    MPGECheck(MPGE_setConfigure(MC_INPUTFILE, MC_INPDEV_FILE, LongInt(PChar(WavFileName))));
    MPGECheck(MPGE_setConfigure(MC_OUTPUTFILE, MC_OUTDEV_FILE, LongInt(PChar(MP3FileName))));
    MPGECheck(MPGE_setConfigure(MC_USEPSY, 1, 0));
    //MPGECheck(MPGE_setConfigure(MC_USEPSY, 0, 0));
    MPGECheck(MPGE_setConfigure(MC_EMPHASIS, MC_EMP_NONE, 0));
    MPGECheck(MPGE_setConfigure(MC_ENCODEMODE, MC_MODE_MONO, 0));
    MPGECheck(MPGE_setConfigure(MC_USELPF16, 1, 0));

    MPGECheck(MPGE_setConfigure(MC_USEMMX, 1, 0));
    MPGECheck(MPGE_setConfigure(MC_USEKNI, 1, 0));
    MPGECheck(MPGE_setConfigure(MC_USESPC1, 1, 0));
    MPGECheck(MPGE_setConfigure(MC_USESPC2, 1, 0));
    
    if DoubleSpeed then
      begin
        MPGE_setConfigure(MC_INPFREQ, 44100, 0);
        MPGECheck(MPGE_setConfigure(MC_BITRATE, 64, 0));
      end  
    else
      MPGECheck(MPGE_setConfigure(MC_BITRATE, 32, 0));

    MPGECheck(MPGE_detectConfigure);
    try
      // �������� ����� ���������� �������
      MPGECheck(MPGE_getConfigure(MG_COUNT_FRAME, frameCount));

      frameIdx := 0;
      while True do
      begin
        rVal := MPGE_processFrame;
        case rVal of
          ME_NOERR:
            begin
            end;
          ME_EMPTYSTREAM:
            begin
              if Assigned(FOnMP3Progress) then
                FOnMP3Progress(Self, (frameIdx *100) div frameCount);
              Break;
            end;
        else
          MPGECheck(rVal);
        end;

        if (frameIdx mod 32) = 0 then
          begin
            if Assigned(FOnMP3Progress) then
              FOnMP3Progress(Self, (frameIdx *100) div frameCount);
          end;
        Inc(frameIdx);
      end;

    finally
      MPGE_closeCoder;
    end;
  finally
    MPGE_endCoder;
  end;
end;

{ TDTB_MP3Encoder }

constructor TDTB_MP3Encoder.Create;
begin
  InitGogoDefs;
end;

destructor TDTB_MP3Encoder.Destroy;
begin
  DeallocateGogoDLL;
  inherited;
end;

procedure TDTB_MP3Encoder.SetOnMP3Progress(
  const Value: TEncodeWavToMP3ProgressEvent);
begin
  FOnMP3Progress := Value;
end;

end.
