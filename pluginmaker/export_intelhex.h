#ifdef __cplusplus
extern "C" {
#endif

bool export_intel_hex_pic24_constdata(LPCTSTR filename,DWORD hex_start,unsigned char* p, DWORD areasize);
bool export_intel_hex(LPCTSTR filename,unsigned long hex_start,unsigned char* p, unsigned long areasize);

#ifdef __cplusplus
}
#endif
