/*
	Creates VS1053b plugin file for Audio Book player/recorder/talking clock
	(c) Elecgseste Ltd 2010


	bootloader placed at 0x0000 ... 0x03FE
	Settings uses one row of flash memory

	=== How bytes aligned in intel hex file ===

	== instructions packing ==

	24-bit data presented in hex file always as 'xxxxxx00' substring,
	LSB first. Address field is PIC's real load adress multiplied by 2. 
	Maximum  amount of 24-bit words per one line in hex file equals 4. Example:
	
	Addr	opcode
	----	------
	2B64	FA0000
	2B66	205351
	2B68	E90081
	2B6A	3AFFFE

	would be translated into ':1056c8000000fa00515320008100e900feff3a0073' string.

	Where:
	10 - bytes count (4 24-bit padded by 00 words)
	56c8 - load address (actualy = 56c8/2 = 0x2B64 in PIC's address space)
	00 - record type, 'data'
	0000fa00 51532000 8100e900 feff3a00 - data, 0xfa0000, 0x205351, 0xE90081 and 0x3AFFFE
	73 - checksum

	Erase block size of PIC24FJ is 8 rows, 1536 bytes (3*0x200, 0x200=512 instructions). Thus the 
	erase area in hex file address space is 2*0x200 = 0x400 bytes
	
	So data area must be aligned to 0x200 in PIC's address space, or 0x400 in HEX file address space


	== constant data packing ==
	example:
	_testdata1 in MAP file located at 0x00058B and contains bytes 0x01,0x02,0x03,0x04,0x05,0x06,
	0x07,0x08,0x09,0x0A,0x0B,0x0C,0x0D,0x0E,0x0F,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17

	all the data placed sequentially in flash instruction storage and does not use bits 16..23 at all
	(padded by 00); Int16 data aligned to even PIC's (i.e 4 byte boundry in HEX address space), int8 are not
*/

#include "stdafx.h"
#include "vs_plugins.h"
#include "export_intelhex.h"

#define PM_ROW_SIZE 64*8
//#define CM_ROW_SIZE 8
#define DEFAULT_LOAD_BLOCK_NUMBER	16	/* be careful! */

//#define OUTFILENAME	TEXT("PLG201.BIN")
#define OUTFILENAME	TEXT("PLG202.HEX")
#define DEFAULT_PIC24_LOCATION	(2*PM_ROW_SIZE)*DEFAULT_LOAD_BLOCK_NUMBER


#define ALIGN_WORD(x)	((x&1)?x+1:x) 
#define alignedsizeof(x)	ALIGN_WORD(sizeof(x))

#define APPENSIZE(x)	fullsize += alignedsizeof(x);


int ExportForPICsFlash()
{
	DWORD hex_start = DEFAULT_PIC24_LOCATION * 2;
	DWORD fullsize = sizeof(tPluginTableFlash);
	APPENSIZE(patch_w_flac)
	APPENSIZE(ima_encoder_fix)
	APPENSIZE(atempochanger)
	APPENSIZE(dtempochanger)
	
	APPENSIZE(venc44k2q05)
	APPENSIZE(venc16k1q05)

	// updated
	APPENSIZE(venc16k1q10)
	APPENSIZE(venc8k1q05)
	APPENSIZE(venc44k2q00)
	APPENSIZE(venc16k2q05)
	APPENSIZE(admixstereo)
	unsigned char* p = (unsigned char*)malloc(fullsize); // include crc
	if(p)
	{
		memset(p,0,fullsize);
		unsigned char* p1 = p+alignedsizeof(tPluginTableFlash);
		DWORD offset = sizeof(tPluginTableFlash);

#define APPENDDATA_A(idx,object)	memcpy(p1,object,sizeof(object)); ((pPluginTableFlash)p)->elements[idx].atable = (tPIC24PointerHolder)(hex_start + (p1-p)); p1+=sizeof(object); offset+=sizeof(object); if(sizeof(object)%2) {p1+=1; offset++;}
#define APPENDDATA_D(idx,object)	memcpy(p1,object,sizeof(object)); ((pPluginTableFlash)p)->elements[idx].dtable = (tPIC24PointerHolder)(hex_start + (p1-p)); p1+=sizeof(object); offset+=sizeof(object); if(sizeof(object)%2) {p1+=1; offset++;}((pPluginTableFlash)p)->elements[idx].arraysize = (tPIC24size_t)sizeof(object)/2
		
		APPENDDATA_D(0,patch_w_flac);
		APPENDDATA_D(1,ima_encoder_fix);
		
		APPENDDATA_A(2,atempochanger);
		APPENDDATA_D(2,dtempochanger);

		APPENDDATA_D(3,venc16k2q05);	//stereo low
		APPENDDATA_D(4,venc44k2q00);	//stereo middle
		APPENDDATA_D(5,venc44k2q05);	//stereo high

		APPENDDATA_D(6,venc8k1q05);		//mono low
		APPENDDATA_D(7,venc16k1q05);	//mono middle
		APPENDDATA_D(8,venc16k1q10);	//mono high
		
		APPENDDATA_D(9,admixstereo);
		
		assert(p1 == p + fullsize);

		bool result = export_intel_hex_pic24_constdata(OUTFILENAME,hex_start,p,fullsize);
		free(p);
		return result ? 0 : 2;
	}
	return 1;
}

//DWORD ExportUncompressedPlugin(unsigned char* p, unsigned char* atable, unsigned short* dtable

int ExportForEeerpom()
{
	struct {
		const unsigned char * atable;
		const unsigned short * dtable;
		const unsigned short * comprssed_table;
		const unsigned int arraysize;
	} work[10] =
	{
		{NULL,NULL,patch_w_flac,PATCH_W_FLAC_PLUGIN_SIZE},
		{NULL,NULL,ima_encoder_fix,IMA_ENCCODER_PATCH_SIZE},
		{atempochanger,dtempochanger,NULL,TEMPOCHANGER_SIZE},
		{NULL,NULL,venc16k2q05,venc16k2q05_SIZE},
		{NULL,NULL,venc44k2q00,venc44k2q00_SIZE},
		{NULL,NULL,venc44k2q05,venc44k2q05_SIZE},
		{NULL,NULL,venc8k1q05,venc8k1q05_SIZE},
		{NULL,NULL,venc16k1q05,venc16k1q05_SIZE},
		{NULL,NULL,venc16k1q10,venc16k1q10_SIZE},
		{NULL,NULL,admixstereo,admixstereo_SIZE}
	};
	
	DWORD hex_start = 0;
	DWORD fullsize = alignedsizeof(tPluginDirEepromElement)*(ARRAYSIZE(work)+1);
	DWORD i;
	
	for(i=0; i < ARRAYSIZE(work); i++)
	{
		if(work[i].comprssed_table)
			fullsize += ALIGN_WORD(work[i].arraysize*2);
		else
			fullsize += ALIGN_WORD(work[i].arraysize*3);
	}
	unsigned char* p = (unsigned char*)malloc(fullsize); // include crc
	if(p)
	{
		memset(p,0,fullsize);
		pPluginDirEepromElement pPluginDir = (pPluginDirEepromElement)p;
		unsigned char* p1 = p+alignedsizeof(tPluginDirEepromElement)*(ARRAYSIZE(work)+1);
		DWORD offset = sizeof(tPluginDirEepromElement) * (ARRAYSIZE(work)+1);
		pPluginDir[ARRAYSIZE(work)].offset = -1;
		pPluginDir[ARRAYSIZE(work)].arraysize = -1;
		pPluginDir[ARRAYSIZE(work)].type = PLUGIN_TYPE_END_OF_LIST;

		for(i=0; i < ARRAYSIZE(work); i++)
		{
			pPluginDir[i].offset = offset;
			pPluginDir[i].arraysize = work[i].arraysize;
			if(work[i].comprssed_table)
			{
				pPluginDir[i].type = PLUGIN_TYPE_COMPRESSED_PLUGIN;
				for(DWORD j = 0; j < work[i].arraysize; j++)
				{
					*(p1++) = (work[i].comprssed_table[j]) & 0xff;
					*(p1++) = (work[i].comprssed_table[j] >> 8) & 0xff;
					offset +=2;
				}
			} else
			{
				pPluginDir[i].type = PLUGIN_TYPE_UNCOMPRESSED_PLUGIN;
				for(DWORD j = 0; j < work[i].arraysize; j++)
				{
					*(p1++) = work[i].atable[j];
					*(p1++) = (work[i].dtable[j]) & 0xff;
					*(p1++) = (work[i].dtable[j] >> 8) & 0xff;
					offset +=3;
				}
				offset = ALIGN_WORD(offset);
				//if(((int)p1) & 1) p1++;
			}
		}
		assert(p1 == p + fullsize);
		bool result = export_intel_hex(OUTFILENAME,hex_start,p,fullsize);
		free(p);
		return result ? 0 : 2;
	}
	return 1;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//return ExportForPICsFlash();
	return ExportForEeerpom();
}

