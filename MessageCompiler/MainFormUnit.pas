unit MainFormUnit;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  JclFileUtils,
  Math,
  SpeechLib_TLB,
  //SpError,
  EG_DTBGogoUtils;

type
	  TMyNotifyEvent = procedure(Current,Total:integer) of object;	

  TMainForm = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    btCompile: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure btCompileClick(Sender: TObject);
  private
    { Private declarations }
    procedure OnProgress(Current,Total:integer);
  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;
procedure CompileMessages(infilename,h_file_name,msg_file_name:string;
	OnProgress:TMyNotifyEvent);

procedure SpeakTextToWavFile(Text: String; WaveFileName: String);


implementation
	uses strutils;

{$R *.dfm}

const
  SECTOR_SIZE = 512;
  SECTORS_ROOT = 6;


type
  TMessageData = packed record
    MessageID: Word;
    StartSector: Word;
    CountOfSectors: Word;
  end;

type
  TMessageAllocationTable = packed record
    CountOfMessages: Word;
    Messages: array[0..(SECTOR_SIZE*SECTORS_ROOT-2) div SizeOf(TMessageData) - 1] of TMessageData;
  end;

procedure TMainForm.Button1Click(Sender: TObject);
var
  SrcDirPath: String;
  DestFile: String;
  FileList: TStringList;
  i: Integer;
  FileSize: Integer;
  FileSectors: Integer;
  MessagesFileSize: Integer;
  MS: TMemoryStream;
  FS: TFileStream;
  ZeroBuff: array[0..SECTOR_SIZE*SECTORS_ROOT-1] of Byte;
  MAT: TMessageAllocationTable;
  MsgID: String;
begin
  if SizeOf(TMessageAllocationTable) > SECTOR_SIZE*SECTORS_ROOT then
    raise Exception.Create('������ ��������� TMessageAllocationTable ��������� ������ �������!');
  SrcDirPath := ExtractFilePath(Application.ExeName) + 'MP3Messages\';
  DestFile := ExtractFilePath(Application.ExeName) + 'MESSAGES.BIN';
  FileList := TStringList.Create;
  try
    AdvBuildFileList(SrcDirPath+'*.mp3', faAnyFile, FileList, amAny,
      [flFullNames, flRecursive], '', nil);
    FileList.Sort;
    Memo1.Lines.Assign(FileList);

    FillChar(ZeroBuff, SECTOR_SIZE*SECTORS_ROOT, 0);
    FillChar(MAT, SizeOf(TMessageAllocationTable), 0);
    MAT.CountOfMessages := FileList.Count;
    MS := TMemoryStream.Create;
    try
      MessagesFileSize := SECTOR_SIZE*SECTORS_ROOT; // � ������ �������� ���������
      MS.WriteBuffer(ZeroBuff, SECTOR_SIZE*SECTORS_ROOT);
      for i := 0 to FileList.Count-1 do
      begin
        MsgID := UpperCase(Copy(ExtractFileName(ChangeFileExt(FileList[i], '')), 1, 4));
        MAT.Messages[i].MessageID := StrToInt(MsgID);
        MAT.Messages[i].StartSector := MS.Position div SECTOR_SIZE;
        FileSize := FileGetSize(FileList[i]);
        FileSectors := Ceil(FileSize / SECTOR_SIZE);
        MessagesFileSize := MessagesFileSize+FileSectors*SECTOR_SIZE;
        FS := TFileStream.Create(FileList[i], fmOpenRead);
        try
          MS.CopyFrom(FS, 0);
          MS.WriteBuffer(ZeroBuff, Ceil(FileSize / SECTOR_SIZE)*SECTOR_SIZE - FileSize);
        finally
          FS.Free;
        end;
        MAT.Messages[i].CountOfSectors := FileSectors;
      end;
      MS.Position := 0;
      MS.Write(MAT, SizeOf(TMessageAllocationTable));
      Caption := IntToStr(MessagesFileSize) + ' ' + IntToStr(MS.Size);
      MS.SaveToFile(DestFile);
    finally
      MS.Free;
    end;
  finally
    FileList.Free;
  end;
end;

procedure TMainForm.Button2Click(Sender: TObject);
var
  SL: TStringList;
  SLX: TStringList;
  i: Integer;
begin
  SL := TStringList.Create;
  try
    SL.LoadFromFile(ExtractFilePath(Application.ExeName) + 'messages.txt');
    for i := 0 to SL.Count-1 do
    begin
      SLX := TStringList.Create;
      try
        SLX.Add(SL[i]);
        SLX.SaveToFile(ExtractFilePath(Application.ExeName) + Format('MP3Messages\%4.4d.txt', [i]));
        SpeakTextToWavFile(SL[i], ExtractFilePath(Application.ExeName) + Format('MP3Messages\%4.4d.wav', [i]));
      finally
        SLX.Free;
      end;
    end;
  finally
    SL.Free;
  end;
end;

procedure TMainForm.Button3Click(Sender: TObject);
var
  MP3Encoder: TDTB_MP3Encoder;
  SrcDirPath: String;
  WavFileList: TStringList;
  i: Integer;
begin
  SrcDirPath := ExtractFilePath(Application.ExeName) + 'MP3Messages\';
  MP3Encoder := TDTB_MP3Encoder.Create;
  try
    WavFileList := TStringList.Create;
    try
      AdvBuildFileList(SrcDirPath+'*.wav', faAnyFile, WavFileList, amAny,
        [flFullNames, flRecursive], '', nil);
      WavFileList.Sort;
      for i := 0 to WavFileList.Count-1 do
      begin
        MP3Encoder.EncodeWavToMP3(WavFileList[i], ChangeFileExt(WavFileList[i], '.mp3'), False);
      end;
    finally
      WavFileList.Free;
    end;
  finally
    MP3Encoder.Free;
  end;
end;

procedure SpeakTextToWavFile(Text, WaveFileName: String);
var
  Voice: TSpVoice;
  cpFileStream: TSpFileStream;
begin
  Voice := TSpVoice.Create(nil);
  try
    // create a wave stream
    cpFileStream := TSpFileStream.Create(nil);
    cpFileStream.Connect;
    try
      // Set output format to selected format
      cpFileStream.Format.Type_ := SpeechAudioFormatType(SAFT22kHz16BitMono);
      // Open the file for write
      cpFileStream.Open(WaveFileName, SSFMCreateForWrite, False);

      // Set output stream to the file stream
      Voice.AllowAudioOutputFormatChangesOnNextSet := False;
      Voice.AudioOutputStream := cpFileStream.DefaultInterface;

      if LowerCase(ExtractFileExt(Text)) = '.wav' then
        begin
          // speak the given text with given flags
          Voice.Speak(Text, SVSFIsFilename);
        end
      else
        begin
          // speak the given text with given flags
          Voice.Speak(Text, 0);
        end;

      // wait until it's done speaking with a really really long timeout.
      // the tiemout value is in unit of millisecond. -1 means forever.
      Voice.WaitUntilDone(-1);

      // Since the output stream was set to the file stream, we need to
      // set back to the selected audio output by calling AudioOutputCB_Click
      // as if user just changed it through UI
      //AudioOutputCBChange(nil);

      // close the file stream
      cpFileStream.Close;
    finally
      cpFileStream.Free;
    end;
  finally
    Voice.Free;
  end;
end;

procedure TMainForm.Button4Click(Sender: TObject);
begin
  SpeakTextToWavFile(Memo1.Text, ExtractFilePath(Application.ExeName) + 'MP3Messages\Test.wav');
end;

procedure Encode(infile:string; outfile:string);
var
	MP3Encoder:TDTB_MP3Encoder;
begin
  MP3Encoder := TDTB_MP3Encoder.Create;
  try
  	MP3Encoder.EncodeWavToMP3(infile, outfile, False);
  finally
    MP3Encoder.Free;
  end;
end;


function hex(c:byte):char;
begin
	c := c and $0f;
    if c>9 then result := chr( c-10+Ord('A')) else
        result :=chr(c + Ord('0'));
end;

procedure conv(buf:pchar;dw:dword);
var d:char;
	i:integer;
    c:integer;
begin
	buf[0] := '0';
	buf[1] := 'x';
    for i := 0 to 7 do
    begin
    	c := (dw shr 28) and $F;
        if c>9 then buf[i+2] := chr( c-10+Ord('A')) else
	        buf[i+2] :=chr(c + Ord('0'));
        dw := dw shl 4;
    end;
    buf[10] := ',';
end;




procedure CompileMessages(infilename,h_file_name,msg_file_name:string;
	OnProgress:TMyNotifyEvent);
var
    SL: TStringList;
    i: Integer;
    FileSize: Int64;
    FileSectors: Integer;
//    MessagesFileSize: Integer;
    MS: TMemoryStream;
    FS: TFileStream;
    ZeroBuff: array[0..SECTOR_SIZE*SECTORS_ROOT-1] of Byte;
    MAT: TMessageAllocationTable;
    MsgID: integer;
    str_id,s,stmp:string;
    n, j1,j2:integer;
    hfile,cfile:TFileStream;
    mp3file:string;
    c_file_name:string;
    reccount: integer;
    Buffer: array[0..15] of byte;
    sBuffer: array[0..255] of char;
    workdir,cstring:string;
begin
	workdir := ExtractFilePath(infilename);
	c_file_name := ChangeFileExt(h_file_name,'.inc');
    FillChar(ZeroBuff, SECTOR_SIZE*SECTORS_ROOT, 0);
    FillChar(MAT, SizeOf(TMessageAllocationTable), 0);
    MAT.CountOfMessages := 0;
    MS := TMemoryStream.Create;
    reccount:=0;
    try
        //MessagesFileSize := SECTOR_SIZE*SECTORS_ROOT; // � ������ �������� ���������
        MS.WriteBuffer(ZeroBuff, SECTOR_SIZE*SECTORS_ROOT);
        MsgID := 0;
        //	inpath := ExtractFilePath(Application.ExeName);
        //    outpath := inpath;
        //const h_file_name = 'msg.h',c_file_name = 'msg.c';
        hfile := TFileStream.Create(h_file_name, fmCreate);
        try
		        stmp := '#ifndef __MP3_MESSAGES_H_'#$d#$a'#define __MP3_MESSAGES_H_'#$d#$a;
    	    	hfile.WriteBuffer(pchar(stmp)^,length(stmp));
                SL := TStringList.Create;
                try
                	try
	                    SL.LoadFromFile(infilename);
                    except on e: exception do
	                    begin
        	                MessageBox(0,pchar(e.Message),'MessageCompiler',
    	                        MB_OK or MB_ICONSTOP);
            	            exit;
                        end
                    end;
                    for i := 0 to SL.Count-1 do
                    begin
                        // parse this line
                        s := Trim(SL.Strings[i]);
                        if Copy(s,1,1) = '#' then continue;
                        j1 := pos(' ',s);
                        j2 := pos(#9,s);
                        if (j1 > 0) AND (j2 > 0) then j1 := min (j1,j2) else
                            if j1 = 0 then j1:=j2;
                        if j2 = 0 then continue;
                        str_id := copy(s,1,j1-1);
                        //seek definition
                        s := trim(copy(s,j1,MAXINT));
                        stmp := '#define ' + str_id + StringOfChar(' ',30-length(str_id)) + IntToStr(MsgID) + #$d#$a;
                        hfile.WriteBuffer(pchar(stmp)^,length(stmp));

                        // strip trailing comments
                        j2 := Pos('#',s); if j2 > 0 then s := copy (s,1, j2-1);

                        if Copy(s,1,1) <> '=' then
                        begin
                            stmp := workdir + 'tmp.wav';
                            SpeakTextToWavFile(s, stmp);
                            mp3file := ChangeFileExt(stmp,'.mp3');
                            if UpperCase(ExtractFileExt(stmp)) = '.WAV' then
                            begin
                                if not FileExists(stmp)
                                    then raise Exception.Create('Cannot find file ' + stmp);
                                Encode(stmp,mp3file);
                            end;
                        end else
                        begin
                            stmp := trim(copy(s,2,MAXINT)); // remove '=' sign
                            if ExtractFileDir(stmp) = '' then
                                stmp := workdir + stmp;
                            mp3file := stmp;

                        end;
                        // convert to MP3
(*
                        mp3file := ChangeFileExt(stmp,'.mp3');
                        if UpperCase(ExtractFileExt(stmp)) = '.WAV' then
                        begin
                        	if not FileExists(stmp)
                            	then raise Exception.Create('Cannot find file ' + stmp);
                            Encode(stmp,mp3file);
                        end;
*)
                        // append
                        MAT.Messages[MsgID].MessageID := MsgID;
                        MAT.Messages[MsgID].StartSector := MS.Position div SECTOR_SIZE;
                        FileSize := FileGetSize(mp3file);
                        FileSectors := Ceil(FileSize / SECTOR_SIZE);
                        //MessagesFileSize := MessagesFileSize+FileSectors*SECTOR_SIZE;
                        try
                            FS := TFileStream.Create(mp3file, fmOpenRead);
                            try
                                MS.CopyFrom(FS, 0);
                                MS.WriteBuffer(ZeroBuff, Ceil(FileSize / SECTOR_SIZE)*SECTOR_SIZE - FileSize);
                            finally
                                FS.Free;
                            end;
                        except on e:exception do
                        	begin
	                        	//stmp := 'Cannot open file ';
    	                        //stmp := stmp + mp3file;
	    	                   	//MessageBox(pchar(stmp),MB_OK or MB_ICONSTOP);
                                MessageBox(0,pchar(e.Message),'MessageCompiler',
	                                MB_OK or MB_ICONSTOP);
                                exit;
                            end else
						end;
                        MAT.CountOfMessages := MAT.CountOfMessages + 1;
                        MAT.Messages[MsgID].CountOfSectors := FileSectors;
                        MsgID := MsgID + 1;
                        if @OnProgress <> nil then
                            OnProgress(MsgID,SL.Count);
                    end;
                finally
                    SL.Free;
                end;
                if SizeOf(TMessageAllocationTable) > SECTOR_SIZE*SECTORS_ROOT then
                    raise Exception.Create('������ ��������� TMessageAllocationTable ��������� ������ �������!');
	        stmp := '#endif'#$d#$a;
        	hfile.WriteBuffer(pchar(stmp)^,length(stmp));
        finally
        	hfile.Free;
        end;
        MS.Position := 0;
        MS.Write(MAT, SizeOf(TMessageAllocationTable));
		MS.SaveToFile(msg_file_name);
        cfile := TFileStream.Create(c_file_name, fmCreate);
        try
        	MS.Position :=0;
            FileSize := MS.Size;
            j1 := Ceil(FileSize / SECTOR_SIZE)*SECTOR_SIZE div SIzeof(Buffer);
            sBuffer[80] := #$d;
            sBuffer[81] := #$a;
            sBuffer[82] := #0;
            while j1 > 0 do
            begin
                n:= MS.Read(Buffer,SIzeof(Buffer));
                if (n<SIzeof(Buffer)) then
                    FillChar(pchar(@Buffer)[n],SIzeof(Buffer)-n,0);
                for j2 := 0 to SIzeof(Buffer) - 1 do
                begin
                    sBuffer[j2*5] := '0';
                    sBuffer[j2*5+1] := 'x';
                    sBuffer[j2*5+2] := hex(Buffer[j2] shr 4);
                    sBuffer[j2*5+3] := hex(Buffer[j2]);
                    sBuffer[j2*5+4] := ',';
                end;
            	cfile.WriteBuffer(sBuffer,82);
                j1 := j1 - 1;
                i := 1;
            end;
            cfile.Size := cfile.Size - 3; 
        finally
            cfile.Free;
        end;
    finally
		MS.Free;
    end;
end;


procedure TMainForm.OnProgress(Current,Total:integer);
begin
	if (Current < Total -1) then
    	Caption := 'Current line '+ IntToStr(Current) + ' of ' + IntToStr(Total)
    else
        Caption := 'Done';
    Application.ProcessMessages();
end;

procedure TMainForm.btCompileClick(Sender: TObject);
begin
	btCompile.Enabled := false;
    ExitCode := 1;
	try
		CompileMessages('messages.msg','msg.h','MSG102.BIN',self.OnProgress);
        ExitCode := 0;
    finally
    	btCompile.Enabled := true;
    end;
end;

end.
