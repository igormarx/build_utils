 // buildinc.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "ver.h"

#define DEFINE_STR	TEXT("#define\t")
#define ZERO_TRAIL_STR	TEXT("\t0\n")

typedef std::basic_string<TCHAR>	t_string;
typedef std::basic_string<wchar_t>	w_string;
typedef std::basic_string<char>		a_string;
typedef std::basic_istream<TCHAR, std::char_traits<TCHAR> > t_istream;
typedef std::basic_ifstream<TCHAR, std::char_traits<TCHAR> > t_ifstream;
typedef std::basic_ofstream<TCHAR, std::char_traits<TCHAR> > t_ofstream;

#ifdef UNICODE
#define cout	std::wcout
#define cerr	std::wcerr
#else
#define cout	std::cout
#define cerr	std::cerr
#endif

typedef enum {
	eIncrement,
	eReset
} enAction;
typedef struct {
	t_string	var_name;
	enAction	action;
} t_record;

t_string infile, outfile;
 
std::vector<t_record> args;

bool CheckOption (const TCHAR* ptr, const TCHAR option)
{
	TCHAR c = *(ptr++);
	if (c != '-' && c != '/')
		return false;
	c = *(ptr++);
	if (c > 0x80)
		return false;
	if ((c != option) && (toupper(c) != option))
		return false;
	return ':' == *(ptr);
}

bool parse_cmd(int argc, TCHAR* argv[])
{
	for( int i = 1; i < argc; i++)
	{
		if (CheckOption(argv[i],'f')) // input file
		{
			if (infile.length() == 0)
				infile = &argv[i][3];
			else
			{
				cerr << TEXT("Error: only one input file allowed.\n");
				return false;
			}
		} else if (CheckOption(argv[i],'o')) // input file
		{
			if (outfile.length() == 0)
				outfile = &argv[i][3];
			else
			{
				cerr << TEXT("Error: only one output file allowed.\n");
				return false;
			}
		} else if (CheckOption(argv[i],'i')) // increment value
		{
			t_record rec;
			rec.action = eIncrement;
			rec.var_name = &argv[i][3];
			args.push_back(rec);
		} else if (CheckOption(argv[i],'z')) // increment value
		{
			t_record rec;
			rec.action = eIncrement;
			rec.var_name = &argv[i][3];
			args.push_back(rec);
		}
		//_tprintf(TEXT("%s\n"), argv[i]);
	}
	if (infile.length() == 0)
	{
		cerr << TEXT("No input file specified");
		return false;
	}
	if (!args.size())
	{
		cerr << TEXT("-z or -i options are need");
		return false;
	}
	return true;
}

void help()
{
	TCHAR message[512];
	_stprintf (message,
		TEXT("(c) Khlebnikov Igor, 2009 - 2010, Freeware.\n")
		TEXT("Build increment utility, ver %d.%d.%d.%d.\r\nUsage: buildinc.exe <option1> <option2> ...\n\n")
		TEXT("Options:\n")
		TEXT("-f:<filename>   header file to be processed (ANSI encoded)\n")
		TEXT("-o:<filename>   additional output copy of the header file (ANSI encoded)\n")
		TEXT("-i:<varname>    increment of definitions declared with #define\n")
		TEXT("-z:<varname>    reset to zero declared with #define\n"),VER_MAJOR,VER_MINOR,VER_RELEASE,VER_BUILD
		);
	cout << message;
}

int _tmain(int argc, _TCHAR* argv[])
{
	t_ifstream ifile;
	//t_ofstream ofile;
	std::wofstream ofile;
	bool bConverted = false;
	bool bAdditionalCopy = false;

	if (argc < 2 || !parse_cmd(argc, argv))
	{
		help();
		//Sleep(5000);
		return 1;
	};

	bool bSuccess = false;
	size_t i, n = args.size();
	
	ifile.open(infile.c_str(), std::ios_base::in);
	if (!ifile.is_open())
	{
		cerr << "Cannot open input file file" << infile.c_str() << TEXT("\n");
		//Sleep(1000);
		return 1;
	} else
	{
		bAdditionalCopy = (outfile.length()!=0);
		if (!bAdditionalCopy)
			outfile = infile + TEXT(".~");
		ofile.open(outfile.c_str(), std::ios_base::out | std::ios_base::trunc);
		if (ofile.is_open())
		{
			TCHAR line[1024], *tok, *var, *val, *tail;
			t_string oldline, newline;
			int value;

			while (!ifile.eof() && !ifile.bad())
			{
				newline.clear();
				oldline.clear();
				ifile.getline(line, ARRAYSIZE(line)-1);
				oldline.append(line);
				tok = _tcstok(line, TEXT(" \t"));
				if (tok && _tcscmp(TEXT("#define"),tok)==0)
				{
					var = _tcstok(NULL, TEXT(" \t\n"));
					if (var)
					{
						val = _tcstok(NULL, TEXT(" \t\n"));
						if(val)
						{
							tail = val + _tcslen(val);
							for (i = 0; i < n; i++)
							{
								const TCHAR *varname = args[i].var_name.c_str();
								if (_tcscmp(varname, var) == 0)
								{
									// try to parse value as decimal or hex
									if(_stscanf(val,TEXT("%i"),&value) == 1)
									{
										switch(args[i].action)
										{
										case eIncrement: value++; break;
										case eReset: value = 0; break;
										}
										//build new line
										TCHAR* format = TEXT("%d");
										TCHAR val_str[17];

										if (*val == '0')
										{
											if (*(val+1) == 'x')
												format = TEXT("0x%X");
											else if (*(val+1) == 'X')
												format = TEXT("0x%X");
											else
												if (_tcslen(val) > 1)
													format = TEXT("0%o");
										}
										_stprintf(val_str,format, value);
										newline.append(oldline.c_str(), val - line);
										newline.append(val_str, _tcslen(val_str));
										newline.append(oldline.c_str() + (tail - line));
										bConverted = true;
									}
								}
							}
						}
					}
				}
				if (!newline.length())
					newline = oldline;
				ofile.write(newline.c_str(),newline.length());
				if (!ifile.eof())
					ofile.write(TEXT("\n"),1);
				bConverted = bConverted && !ofile.bad();
			}// while
			ofile.close();
		} else
		{
			cerr << "Cannot create output file" << outfile.c_str() << TEXT("\n");
			//Sleep(1000);
			return 1;
		}
		ifile.close();
		/*if(bConverted && )
		{
			if(!CopyFile(outfile.c_str(),infile.c_str(), false))
			{
				cerr << "Cannot place new copy of file!\n";
				return 1;
			}
		}
		*/
		if(!CopyFile(outfile.c_str(),infile.c_str(), false))
		{
			cerr << "Cannot place new copy of file!\n";
			//Sleep(1000);
			return 1;
		}
		if (!bAdditionalCopy)
		{
			DeleteFile(outfile.c_str());
		}
	}
	return 0;
}


