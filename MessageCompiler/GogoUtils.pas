unit GogoUtils;

interface

uses
  SysUtils, GogoDefs;

type
  EMpgeError = Exception;

function MpgeErrorStr(Res: MERET): String;
procedure MPGECheck(Res: MERET);

implementation

function MpgeErrorStr(Res: MERET): String;
begin
  case Res of
    ME_NOERR: Result := 'ME_NOERR';
    ME_EMPTYSTREAM: Result := 'ME_EMPTYSTREAM';
    ME_HALTED: Result := 'ME_HALTED';

    ME_INTERNALERROR: Result := 'ME_INTERNALERROR';
    ME_PARAMERROR: Result := 'ME_PARAMERROR';
    ME_NOFPU: Result := 'ME_NOFPU';
    ME_INFILE_NOFOUND: Result := 'ME_INFILE_NOFOUND';
    ME_OUTFILE_NOFOUND: Result := 'ME_OUTFILE_NOFOUND';
    ME_FREQERROR: Result := 'ME_FREQERROR';
    ME_BITRATEERROR: Result := 'ME_BITRATEERROR';
    ME_WAVETYPE_ERR: Result := 'ME_WAVETYPE_ERR';
    ME_CANNOT_SEEK: Result := 'ME_CANNOT_SEEK';
    ME_BITRATE_ERR: Result := 'ME_BITRATE_ERR';
    ME_BADMODEORLAYER: Result := 'ME_BADMODEORLAYER';
    ME_NOMEMORY: Result := 'ME_NOMEMORY';
    ME_CANNOT_SET_SCOPE: Result := 'ME_CANNOT_SET_SCOPE';
    ME_CANNOT_CREATE_THREAD: Result := 'ME_CANNOT_CREATE_THREAD';
    ME_WRITEERROR: Result := 'ME_WRITEERROR';
  else
    Result := 'Unknown MPGE error!';
  end;
end;

procedure MPGECheck(Res: MERET);
begin
  if Res <> 0 then
    raise EMpgeError.Create(MpgeErrorStr(res));
end;


end.
