// sdcard_finer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "usbfind.h"


int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<usbdisk_t> device_list;
	win32_enum_usb_disks(device_list);
	size_t i, n = device_list.size();
	SetEnvironmentVariableW(L"SDCARDDRIVE", L"");
	if( !n )
		return 1;
	usbdisk_t & selected = device_list[0];
	for(i = 0; i < n; i++)
	{
		if(device_list[i].Device_FriendlyName.compare(L"SD Card") == 0)
			selected = device_list[i];
		if(device_list[i].Device_FriendlyName.compare(L"SDHC Card") == 0)
			selected = device_list[i];
		if(device_list[i].Device_FriendlyName.compare(L"SDXC Card") == 0)
			selected = device_list[i];
	}
	wprintf(L"%c:", selected.driveLetter);
	return 0;
}
