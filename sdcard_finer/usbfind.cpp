#include "usbfind.h"
#include <windows.h>
#include <initguid.h>
#include <Devpropdef.h>
#include <Devpkey.h>
#include <assert.h>
#include <setupapi.h>
#include <Cfgmgr32.h>


// ���� ������������� <drive letter> �  <USB device> ����������� � ���, ��� ��� ����� ����� ������� 
// DeviceIoControl(..., IOCTL_STORAGE_GET_DEVICE_NUMBER, ..., ..., &sdn, sizeof(sdn), &dwBytesReturned, NULL)
// ���� sdn ����������, �� <drive letter> ������������� <USB device> �� ���������� �������

typedef struct
{
	wchar_t driveLetter;
	STORAGE_DEVICE_NUMBER sdn;
	wchar_t szDosDeviceName[MAX_PATH];
} dosletter_t;


// This is the GUID for the USB device class
// {A5DCBF10-6530-11D2-901F-00C04FB951ED}:
DEFINE_GUID(GUID_DEVINTERFACE_USB_DEVICE,
	0xA5DCBF10L, 0x6530, 0x11D2, 0x90, 0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED); // "����������� USB"


// {53F56307-B6BF-11D0-94F2-00A0C91EFB8B}
DEFINE_GUID(GUID_DEVINTERFACE_DISK,
	0x53F56307L, 0xB6BF, 0x11D0, 0x94, 0xF2, 0x00, 0xA0, 0xC9, 0x1E, 0xFB, 0x8B); // "�������� ����������"

std::wstring GetDevPropertyStrW(HDEVINFO hDevInfo, PSP_DEVINFO_DATA pDevData, const DEVPROPKEY * pDevPropKey)
{
	std::wstring retval;
	PBYTE PropertyBuffer;
	DWORD dwSize = 0, err;
	DEVPROPTYPE DevPropType = DEVPROP_TYPE_STRING;
	SetupDiGetDeviceProperty(hDevInfo, pDevData, pDevPropKey, &DevPropType, NULL, 0, &dwSize, 0);
	err = GetLastError();
	if(err != ERROR_NOT_FOUND)
	{
		if(ERROR_INSUFFICIENT_BUFFER == err)
		{
			PropertyBuffer = (PBYTE)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			if(PropertyBuffer)
			{
				if(SetupDiGetDeviceProperty(hDevInfo, pDevData, pDevPropKey, 
					&DevPropType, PropertyBuffer, dwSize, &dwSize, 0))
				{
					//retval.append((wchar_t*)PropertyBuffer, dwSize);
					retval = (wchar_t*)PropertyBuffer;
				}
				HeapFree(GetProcessHeap(), 0, PropertyBuffer);
			}
		} else
		{
			assert(0);
		}
	}
	return retval;
}

void win32_enum_drive_letters(std::vector<dosletter_t> & doletters_list)
{
	//QueryDosDevice
	DWORD drives = GetLogicalDrives();
	int i;
	wchar_t szRootPath[] = L"X:\\";   // "X:\"  -> for GetDriveType
	wchar_t szDevicePath[] = L"X:";   // "X:"   -> for QueryDosDevice
	wchar_t szVolumeAccessPath[] = L"\\\\.\\X:";   // "\\.\X:"  -> to open the volume

	for(i=0; i < 26; i++) if((1 << i) && drives)
	{
		dosletter_t dosLetter;
		dosLetter.driveLetter = L'A' + i;
		szRootPath[0] = dosLetter.driveLetter;
		szDevicePath[0] = dosLetter.driveLetter;
		szVolumeAccessPath[4] = dosLetter.driveLetter;
		HANDLE hVolume = CreateFileW(szVolumeAccessPath, 0, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, NULL, NULL);
		if (hVolume == INVALID_HANDLE_VALUE)
			continue;
		//STORAGE_DEVICE_NUMBER sdn;
		DWORD dwBytesReturned = 0;
		if(DeviceIoControl(hVolume, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &dosLetter.sdn, sizeof(dosLetter.sdn), &dwBytesReturned, NULL))
		{
			UINT DriveType = GetDriveType(szRootPath);
			if(QueryDosDevice(szDevicePath, dosLetter.szDosDeviceName, MAX_PATH))
				doletters_list.push_back(dosLetter);
		}
		CloseHandle(hVolume);
	}
}

int win32_enum_usb_disks(std::vector<usbdisk_t> & device_list)
{
	HDEVINFO                         hDevInfo;
	//PSP_DEVICE_INTERFACE_DETAIL_DATA DevIntfDetailData;
	SP_DEVINFO_DATA                  DevData;
	DWORD dwMemberIdx;
	BOOL error = TRUE;

	std::vector<dosletter_t> doletters_list;
	win32_enum_drive_letters(doletters_list);

	// We will try to get device information set for all USB devices that have a
	// device interface and are currently present on the system (plugged in).
	hDevInfo = SetupDiGetClassDevs(
		&GUID_DEVINTERFACE_DISK, NULL, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		// Prepare to enumerate all device interfaces for the device information
		// set that we retrieved with SetupDiGetClassDevs(..)
		dwMemberIdx = 0;
		for(;;)
		{
			DevData.cbSize = sizeof(DevData);
			if(!SetupDiEnumDeviceInfo(hDevInfo, dwMemberIdx++, &DevData))
				break;
			// DevData has device information element
			//SetupDiGetDeviceInfoListDetail(
			// devnode

			//DEVPKEY_Device_DeviceDesc - ��, ��� �� ����� � ���������� ��������� (��������, ���->��������� �� ����, "�������� USB-������������")

			//DEVPKEY_Device_DeviceDesc: "�������� ����������" -> "�������� ����������"
			//"��������� ���" -> "�������� ����������"
			//"����� ����������" -> "DiskDrive"
			//"������� ��� ������" -> "DiskDrive"
			//"�������� ���" -> "Cruitial_CT... ATA Device", "SD Card"
			//"��������� ���" -> "Cruitial_CT... ATA Device", "SD Card"
			//DEVPKEY_Device_EnumeratorName: "�������������" -> "IDE", "USBSTOR"
			//"��������� DevNode" �������� ����� �������� ���� DN_NT_STARTED (����� ������)
			//"��������� DevNode" SD ����� �������� ���� DN_NT_STARTED � DN_NT_REMOVABLE (����� ������)

			//������� ������ "���������" �� ������� "����" ���� �������. ���������� DOS �����, ��������: "��� (F:) ������� 1936 ��"
			usbdisk_t Disk;
			//DEVPROP_TYPE_STRING
			Disk.Device_DeviceDesc = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_DeviceDesc);
			Disk.Device_Service = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_Service);
			Disk.Device_Class = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_Class);
			Disk.Device_Driver = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_Driver);
			Disk.Device_Manufacturer = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_Manufacturer);
			Disk.Device_FriendlyName = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_FriendlyName);
			//Disk.Device_LocationInfo = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_LocationInfo);
			Disk.Device_PDOName = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_PDOName);
			//Disk.Device_UINumber = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_UINumber);
			//Disk.Device_UINumberDescFormat = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_UINumberDescFormat);
			Disk.Device_EnumeratorName = GetDevPropertyStrW(hDevInfo, &DevData, &DEVPKEY_Device_EnumeratorName);
			
			/*
			Disk.Device_LegacyBusType = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_LegacyBusType);
			Disk.Device_BusNumber = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_BusNumber);
			Disk.Device_DevType = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_DevType);
			Disk.Device_Characteristics = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_Characteristics);
			Disk.Device_Address = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_Address);
			Disk.Device_RemovalPolicy = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_RemovalPolicy);
			Disk.Device_RemovalPolicyDefault = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_RemovalPolicyDefault);
			Disk.Device_RemovalPolicyOverride = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_RemovalPolicyOverride);
			Disk.Device_InstallState = GetDevPropertyUINT32(hDevInfo, &DevData, &DEVPKEY_Device_InstallState);
			*/

			//Device_LocationPaths,          0xa45c254e, 0xdf1c, 0x4efd, 0x80, 0x20, 0x67, 0xd1, 0x46, 0xa8, 0x50, 0xe0, 37);    // DEVPROP_TYPE_STRING_LIST
			///////////////////////////////
			DWORD dwSize, err;
			// #1
			SP_DEVINFO_LIST_DETAIL_DATA DeviceInfoSetDetailData;
			DeviceInfoSetDetailData.cbSize = sizeof(SP_DEVINFO_LIST_DETAIL_DATA);
			SetupDiGetDeviceInfoListDetail(hDevInfo, &DeviceInfoSetDetailData);
			err = GetLastError();
			assert(ERROR_SUCCESS == err);

			// #5
			wchar_t DeviceInstanceId[200];
			SetupDiGetDeviceInstanceId(hDevInfo, &DevData, DeviceInstanceId, sizeof(DeviceInstanceId), NULL);
			//DeviceInstanceId = "IDE\DISKCRUCIAL_CT250MX200SSD1__________________MU01____\5&2F317EF6&0&0.0.0"
			assert(ERROR_SUCCESS == GetLastError());
			
			// 9 SetupDiCreateDeviceInfoListExW(0x0000000000000000:Bad pointer, 0x0000000000000000, 0x00000000067AE7C0:"", 0x0000000000000000)	0x0000000004A870F0	0x000007FEFF7C3BB2	SETUPAPI.dll + 0x3BB2	0x00002314	0x00000ACC	0x00000000	RAX=0x0000000000000000, RBX=0xFFFFFFFFFFFFFFFF, RCX=0x0000000000000000, RDX=0x0000000000000000, RSI=0x0000000000000000, RDI=0x0000000000D58088, RFL=0x0000000000000246, RSP=0x00000000067AE6D0, RBP=0x0000000000000000, R8=0x00000000067AE7C0, R9=0x000000000000000	RAX=0x0000000004A870F0, RBX=0xFFFFFFFFFFFFFFFF, RCX=0x211DD14447C00000, RDX=0x0000000000000000, RSI=0x0000000000000000, RDI=0x0000000000D58088, RFL=0x0000000000000246, RSP=0x00000000067AE6D0, RBP=0x0000000000000000, R8=0x0000000000000000, R9=0x000000000000000	0	0	00:40:50:959:835,3	1017	setupapi.dll	SetupDiCreateDeviceInfoListExW	C:\Windows\system32\SETUPAPI.dll	
			HDEVINFO DeviceInfoList = SetupDiCreateDeviceInfoListEx(NULL, NULL /* HWND     hwndParent */, DeviceInfoSetDetailData.RemoteMachineName, NULL);
			assert(ERROR_SUCCESS == GetLastError());
			// 74
			//SetupDiGetClassDevsExW(0x0000000000000000, "USBSTOR\DISK&VEN_MULTIPLE&PROD_CARD_READER&REV_1.00\058F63666485&00", 
			// 0x0000000000000000, 0x00000014, 0x0000000000000000, 0x00000000067AE7C0:"", 0x0000000000000000)	0x0000000004A870F0	0x000007FEF4773924	dmvdsitf.DLL + 0x13924	0x00002314	0x00000ACC	0x00000000	RAX=0x00000000067AE7C0, RBX=0x0000000000000000, RCX=0x0000000000000000, RDX=0x00000000067AEA00, RSI=0x0000000000000000, RDI=0x0000000000D58088, RFL=0x0000000000000246, RSP=0x00000000067AE750, RBP=0x0000000000000000, R8=0x0000000000000000, R9=0x000000000000001	RAX=0x0000000004A870F0, RBX=0x0000000000000000, RCX=0x000007FEFF8AA800, RDX=0x0000000000000000, RSI=0x0000000000000000, RDI=0x0000000000D58088, RFL=0x0000000000000206, RSP=0x00000000067AE750, RBP=0x0000000000000000, R8=0x00000000067AE698, R9=0x000000000000000	0	0	00:40:50:959:797,1	20548	setupapi.dll	SetupDiGetClassDevsExW	C:\Windows\system32\dmvdsitf.DLL	
			HDEVINFO ClassDevs = SetupDiGetClassDevsEx(NULL, DeviceInstanceId, NULL, 
				DIGCF_DEVICEINTERFACE | DIGCF_ALLCLASSES, NULL, DeviceInfoSetDetailData.RemoteMachineName, NULL);
			assert(ERROR_SUCCESS == GetLastError());
			//75
			//SetupDiEnumDeviceInterfaces(0x0000000004A870F0, 0x0000000000000000, 0x000007FEF4762198:{53F56307-B6BF-11D0-94F2-00A0C91EFB8B},
			//	0x00000000, 0x00000000067AE798)	0x0000000000000001	0x000007FEF4773975	dmvdsitf.DLL + 0x13975	0x00002314	0x00000ACC	0x00000000	RAX=0x00000000067AE798, RBX=0x0000000000000000, RCX=0x0000000004A870F0, RDX=0x0000000000000000, RSI=0x0000000004A870F0, RDI=0x0000000000000000, RFL=0x0000000000000246, RSP=0x00000000067AE750, RBP=0x000007FEF4794200, R8=0x000007FEF4762198, R9=0x000000000000000	RAX=0x0000000000000001, RBX=0x0000000000000000, RCX=0x000007FEFF8AA800, RDX=0x0000000000000000, RSI=0x0000000004A870F0, RDI=0x0000000000000000, RFL=0x0000000000000246, RSP=0x00000000067AE750, RBP=0x000007FEF4794200, R8=0x00000000067AE6A8, R9=0x000007FEF479420	0	0	00:40:50:980:394,9	7	setupapi.dll	SetupDiEnumDeviceInterfaces	C:\Windows\system32\dmvdsitf.DLL	
			SP_DEVICE_INTERFACE_DATA DeviceInterfaceData;
			DeviceInterfaceData.cbSize = sizeof(DeviceInterfaceData);
			SetupDiEnumDeviceInterfaces(ClassDevs, NULL, &GUID_DEVINTERFACE_DISK, 0, &DeviceInterfaceData);
			assert(ERROR_SUCCESS == GetLastError());

			// 76	
			//SetupDiGetDeviceInterfaceDetailW(0x0000000004A870F0, 0x00000000067AE798, 0x0000000000000000, 0x00000000, 0x00000000067AE790:0x00000000, 0x0000000000000000)	0x0000000000000000	0x000007FEF47739B2	
			SetupDiGetDeviceInterfaceDetail(ClassDevs, &DeviceInterfaceData, NULL, 0, &dwSize, NULL);
			assert(ERROR_INSUFFICIENT_BUFFER == GetLastError());

			PSP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			if(DeviceInterfaceDetail)
			{
				DeviceInterfaceDetail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
				SetupDiGetDeviceInterfaceDetail(ClassDevs, &DeviceInterfaceData, DeviceInterfaceDetail, dwSize, &dwSize, NULL);
				err = GetLastError();
				assert(ERROR_SUCCESS == err);
				Disk.Device_Path = DeviceInterfaceDetail->DevicePath;
				HANDLE h = CreateFileW(DeviceInterfaceDetail->DevicePath, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
				if(h != INVALID_HANDLE_VALUE)
				{
					STORAGE_DEVICE_NUMBER sdn;
					DWORD dwBytesReturned;
					Disk.readonly = false;
					if(!DeviceIoControl(h, IOCTL_DISK_IS_WRITABLE, 0, 0, 0, 0, &err, 0))
					{
						err = GetLastError();
						if(err == ERROR_WRITE_PROTECT)
						{
							Disk.readonly = true;
							SetLastError(ERROR_SUCCESS);
						}
					}
					DeviceIoControl(h, IOCTL_STORAGE_GET_DEVICE_NUMBER, NULL, 0, &sdn, sizeof(sdn), &dwBytesReturned, NULL);
					//sdn.DeviceNumber = 2: "Disk 2"
					// sdn.DeviceType = 7: FILE_DEVICE_DISK
					// sdn.PartitionNumber = 0
					err = GetLastError();
					assert(ERROR_SUCCESS == err);
					size_t i, n = doletters_list.size();
					for(i=0; i < n; i++)
					{
						if((doletters_list[i].sdn.DeviceNumber == sdn.DeviceNumber) &&
							(doletters_list[i].sdn.DeviceType == sdn.DeviceType))
						{
							Disk.driveLetter = doletters_list[i].driveLetter;
							Disk.DosDeviceName = doletters_list[i].szDosDeviceName;
							device_list.push_back(Disk);
						}
					}
					CloseHandle(h);
				}
			}
			SetupDiDestroyDeviceInfoList(ClassDevs);
		}
		SetupDiDestroyDeviceInfoList(hDevInfo);
	}
	return error;
}

#if 0
int win32_enum_usb_interfaces()
{
	HDEVINFO                         hDevInfo;
	SP_DEVICE_INTERFACE_DATA         DevIntfData;
	PSP_DEVICE_INTERFACE_DETAIL_DATA DevIntfDetailData;
	SP_DEVINFO_DATA                  DevData;

	DWORD dwSize, dwType, dwMemberIdx;
	HKEY hKey;
	BYTE lpData[1024];

	// We will try to get device information set for all USB devices that have a
	// device interface and are currently present on the system (plugged in).
	hDevInfo = SetupDiGetClassDevs(
		&GUID_DEVINTERFACE_USB_DEVICE, NULL, 0, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
	
	if (hDevInfo != INVALID_HANDLE_VALUE)
	{
		// Prepare to enumerate all device interfaces for the device information
		// set that we retrieved with SetupDiGetClassDevs(..)
		DevIntfData.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
		dwMemberIdx = 0;
		
		// Next, we will keep calling this SetupDiEnumDeviceInterfaces(..) until this
		// function causes GetLastError() to return  ERROR_NO_MORE_ITEMS. With each
		// call the dwMemberIdx value needs to be incremented to retrieve the next
		// device interface information.

		SetupDiEnumDeviceInterfaces(hDevInfo, NULL, &GUID_DEVINTERFACE_USB_DEVICE,
			dwMemberIdx, &DevIntfData);

		while(GetLastError() != ERROR_NO_MORE_ITEMS)
		{
			// As a last step we will need to get some more details for each
			// of device interface information we are able to retrieve. This
			// device interface detail gives us the information we need to identify
			// the device (VID/PID), and decide if it's useful to us. It will also
			// provide a DEVINFO_DATA structure which we can use to know the serial
			// port name for a virtual com port.

			DevData.cbSize = sizeof(DevData);
			
			// Get the required buffer size. Call SetupDiGetDeviceInterfaceDetail with
			// a NULL DevIntfDetailData pointer, a DevIntfDetailDataSize
			// of zero, and a valid RequiredSize variable. In response to such a call,
			// this function returns the required buffer size at dwSize.

			SetupDiGetDeviceInterfaceDetail(
				  hDevInfo, &DevIntfData, NULL, 0, &dwSize, NULL);

			// Allocate memory for the DeviceInterfaceDetail struct. Don't forget to
			// deallocate it later!
			DevIntfDetailData = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, dwSize);
			DevIntfDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

			if (SetupDiGetDeviceInterfaceDetail(hDevInfo, &DevIntfData,
				DevIntfDetailData, dwSize, &dwSize, &DevData))
			{
				// Finally we can start checking if we've found a useable device,
				// by inspecting the DevIntfDetailData->DevicePath variable.
				// The DevicePath looks something like this:
				//
				// \\?\usb#vid_04d8&pid_0033#5&19f2438f&0&2#{a5dcbf10-6530-11d2-901f-00c04fb951ed}
				//
				// The VID for Velleman Projects is always 10cf. The PID is variable
				// for each device:
				//
				//    -------------------
				//    | Device   | PID  |
				//    -------------------
				//    | K8090    | 8090 |
				//    | VMB1USB  | 0b1b |
				//    -------------------
				//
				// As you can see it contains the VID/PID for the device, so we can check
				// for the right VID/PID with string handling routines.

				if (NULL != _tcsstr((TCHAR*)DevIntfDetailData->DevicePath, _T("vid_10cf&pid_8090")))
				//something like "\\?\usb#vid_046d&pid_0802#5cbda360#{a5dcbf10-6530-11d2-901f-00c04fb951ed}"
				{
					// To find out the serial port for our K8090 device,
					// we'll need to check the registry:

					hKey = SetupDiOpenDevRegKey(
								hDevInfo,
								&DevData,
								DICS_FLAG_GLOBAL,
								0,
								DIREG_DEV,
								KEY_READ
							);

					dwType = REG_SZ;
					dwSize = sizeof(lpData);
					RegQueryValueEx(hKey, _T("PortName"), NULL, &dwType, lpData, &dwSize);
					RegCloseKey(hKey);

					// Eureka!
					wprintf(_T("Found a device on port '%s'\n"), lpData);
				}
			}

			HeapFree(GetProcessHeap(), 0, DevIntfDetailData);

			// Continue looping
			SetupDiEnumDeviceInterfaces(
				hDevInfo, NULL, &GUID_DEVINTERFACE_USB_DEVICE, ++dwMemberIdx, &DevIntfData);
		}

		SetupDiDestroyDeviceInfoList(hDevInfo);
	}

	return 0;
}
#endif
